import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { REDIRECTS_ROUTES } from 'src/app/@core/constants/config';
import { IMenuItem } from 'src/app/@core/interfaces/menu-item.interface';
import { IMeData } from 'src/app/@core/interfaces/session.interface';
import { AuthService } from 'src/app/@core/services/auth.service';
import shopMenuItems from 'src/assets/@data/menus/shop.json'
import { CartService } from '../../services/cart.service.ts.service';
import { ICart } from '../shopping-cart/shoppin-cart.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  cartItemsTotal!: number;
  menuItems: Array<IMenuItem> = shopMenuItems;
  session: IMeData = {
   status: false
 };

 access = false;
 role: string | undefined;
 userLabel!: string;

  constructor(private authService: AuthService, private cartService: CartService, private router: Router) { 
    this.authService.accessVar$.subscribe((result)=> {
      console.log(result.status)
      this.session = result;
      this.access = this.session.status;
      this.role = this.session.user?.role;
      this.userLabel = `${ this.session.user?.name }`;
    });

    this.cartService.itemsVar$.subscribe((data: ICart) => {
      if (data !== undefined && data !== null) {
        this.cartItemsTotal = data.subtotal;
      }
    });
    
  }

  ngOnInit(): void {
    this.cartItemsTotal = this.cartService.initialize().subtotal;
  }

  open() {
    console.log('navbar open cart');
    this.cartService.open();
  }

  logout() {
    if (REDIRECTS_ROUTES.includes(this.router.url)) {
      // En el caso de encontrarla marcamos para que redireccione
      localStorage.setItem('route_after_login', this.router.url);
    }
    this.authService.resetSession();
    }

  }


