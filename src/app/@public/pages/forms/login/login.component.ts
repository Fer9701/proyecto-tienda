import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ILoginForm, IResultLogin} from 'src/app/@core/interfaces/login.interface';
import { IMeData } from 'src/app/@core/interfaces/session.interface';
import { AuthService } from 'src/app/@core/services/auth.service';
import { basicAlert } from 'src/app/@shared/alerts/toasts';
import { TYPE_ALERT } from 'src/app/@shared/alerts/values.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    login: ILoginForm = {
    email: '',
    password: ''
  };
  constructor(private auth: AuthService, private router: Router) { }


  
  init() {
    this.auth.login(this.login.email, this.login.password).subscribe(
      (result: IResultLogin) => {
        if (result.status) {
          if (result.token !== null) {
            this.auth.setSession(result.token!);
            this.auth.updateSession(result);
            if (localStorage.getItem('route_after_login')) {
              this.router.navigate([localStorage.getItem('route_after_login')]);
              localStorage.removeItem('route_after_login');
              return;
            }
            this.router.navigate(['/home']);
            return;
          }
          basicAlert(TYPE_ALERT.WARNING, result.message);
          return;
        }
        basicAlert(TYPE_ALERT.INFO, result.message);
      }
    );
  }
}

