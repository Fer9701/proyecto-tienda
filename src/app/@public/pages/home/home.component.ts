import { Component, OnInit } from '@angular/core';
import { ICarouselItem } from '@mugan86/ng-shop-ui/lib/interfaces/carousel-item.interface';
import { IProduct } from '@mugan86/ng-shop-ui/lib/interfaces/product.interface';
import { ACTIVE_FILTERS } from 'src/app/@core/constants/filters';
import { AuthService } from 'src/app/@core/services/auth.service';
import { ProductsService } from 'src/app/@core/services/products.service';
import { UsersService } from 'src/app/@core/services/users.service';
import { ApiService } from 'src/app/@graphql/services/api.service';
import { closeAlert, loadData } from 'src/app/@shared/alerts/alerts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  items: ICarouselItem[] = [];
  listOne!: IProduct[];
  listTwo!: IProduct[];
  listThree!: IProduct[];
  loading!: boolean;
  constructor(private products: ProductsService) { }

  ngOnInit(): void {
    this.loading = true;
    loadData('Cargando datos', 'Espera mientras carga la información');
    this.products.getHomePage().subscribe( data => {
      this.listOne = data.ps4;
      this.listTwo = data.topPrice;
      this.listThree = data.pc;
      this.items = this.manageCarousel(data.carousel);
      closeAlert();
      this.loading = false;
    });
  }

  private manageCarousel(list: any) {
    const itemsValues: Array<ICarouselItem> = [];
    list.shopProducts.map((item: any) => {
      itemsValues.push({
        id: item.id,
        title: item.product.name,
        description: item.platform.name,
        background: item.product.img,
        url: '/games/details/'.concat(item.id)
      });
    });
    return itemsValues;
  }
}
