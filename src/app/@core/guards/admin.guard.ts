import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivateChild {
  constructor(private auth: AuthService, private router: Router) {}
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    // Primero comprobar que existe sesión

    if (this.auth.getSession() !== null) {
      console.log('Estamos logueados');
      const dataDecode: any = this.decodeToken();
      console.log(dataDecode);

      // Comprobar que no está caducado el token
      if (dataDecode.exp < new Date().getTime() / 1000) {
        console.log('Sesión caducada');
        return this.redirect();
      }

      // El role del usuario es ADMIN
      if (dataDecode.user.role === 'ADMIN') {
        console.log('Somos administradores');
        return true;
      }else{
      console.log('No eres administrador');}
    }
    console.log('Sesion no iniciada');
    return this.redirect();
  }
  redirect() {
    this.router.navigate(['/home']);
    return false;
  }
  decodeToken() {
    return jwt_decode(this.auth.getSession().token);
  }
}










