import gql from "graphql-tag";


// We use the gql tag to parse our query string into a query document
export const USER_FRAGMENT = gql`
    fragment UserObject on User {
        id
        name
        lastname
        email
        password @include (if: $include)
        registerDate @include (if: $include)
        birthday @include (if: $include)
        role
        active
    }
`;
