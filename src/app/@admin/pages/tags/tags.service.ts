
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/internal/operators/map';
import { ADD_TAG, BLOCK_TAG, MODIFY_TAG } from 'src/app/@graphql/operations/mutation/tag';
import { ApiService } from 'src/app/@graphql/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class TagsService extends ApiService{
  constructor(apollo: Apollo) {
    super(apollo);
  }
  add(tag: string) {
    return this.set(
      ADD_TAG,
      {
        tag
      }, {}).pipe(map( (result: any) => {
        return result.addTag;
      }));
  }
  update(id: string, tag: string) {
    return this.set(
      MODIFY_TAG,
      {
        id,
        tag
      }, {}).pipe(map( (result: any) => {
        return result.updateTag;
      }));
  }

  unblock(id: string, unblock: boolean) {
    return this.set(
      BLOCK_TAG,
      {
        id, unblock
      }, {}).pipe(map( (result: any) => {
        return result.blockTag;
      }));
  }
}
