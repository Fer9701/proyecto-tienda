import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/operators';
import { ADD_GENRE, BLOCK_GENRE, MODIFY_GENRE } from 'src/app/@graphql/operations/mutation/genre';
import { ApiService } from 'src/app/@graphql/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class GenresService extends ApiService{

  constructor(apollo: Apollo) {
    super(apollo);
  }

  add(genre: string) {
    return this.set(
      ADD_GENRE,
      {
        genre
      }, {}).pipe(map( (result: any) => {
        return result.addGenre;
      }));
  }
  update(id: string, genre: string) {
    return this.set(
      MODIFY_GENRE,
      {
        id,
        genre
      }, {}).pipe(map( (result: any) => {
        return result.updateGenre;
      }));
  }

  block(id: string) {
    return this.set(
      BLOCK_GENRE,
      {
        id
      }, {}).pipe(map( (result: any) => {
        return result.blockGenre;
      }));
  }
}